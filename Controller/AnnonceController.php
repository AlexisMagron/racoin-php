<?php

namespace Controller;

class AnnonceController extends AbstractController
{


    public static function verifier_formulaire_annonce($posts, $mdp = true)
    {
        
            if($mdp)
            {

                if(!isset($posts['mdp']) || empty($posts['mdp']))
                    throw new \Exception("Il manque le mot de passe");

                if(strlen($posts['mdp']) < 6)
                    throw new \Exception("Votre mot de passe est trop court");

                if($posts['mdp'] != $posts['confirm_mdp'])
                    throw new \Exception("Les mots de passe ne correspondent pas");
            }

            if(!isset($posts['departement']) || empty($posts['departement']))
                throw new \Exception("Département invalide");

            if(!isset($posts['ville']) || empty($posts['ville']))
                throw new \Exception("Il manque la ville");

            if(!isset($posts['code_postal']) || empty($posts['code_postal']) || !is_numeric($posts['code_postal']))
                throw new \Exception("Code postal invalide");

            if(!isset($posts['pseudo']) || empty($posts['pseudo']))
                throw new \Exception("Il manque votre pseudonyme");

            if(!isset($posts['mail']) || empty($posts['mail']))
                throw new \Exception("Il manque votre e-mail");

            if(!isset($posts['telephone']) || empty($posts['telephone']))
                throw new \Exception("Il manque votre numéro de téléphone");

            if(!isset($posts['titre']) || empty($posts['titre']))
                throw new \Exception("Il manque un titre à votre annonce !");

            if(!isset($posts['categorie']) || empty($posts['categorie']))
                throw new \Exception("Catégorie invalide");

            if(!isset($posts['prix']) || empty($posts['prix']) || !is_numeric($posts['prix']))
                throw new \Exception("Prix invalide !");

            if(!isset($posts['description']))
                $posts['description'] = '';




    }

    public function afficher($id) 
    {

        $this->variables_menu();
        $this->variables_searchbar();

        $a = \Model\Annonce::with('departement')->get()->find($id);



        if($a == null) 
            {
            $tmp = static::$twig->loadTemplate("error.html.twig");
            $tmp->display(array(
                "title_error"=>"Erreur 404",
                "description"=>"Vous essayez d'accéder à une annonce qui n'existe pas."
            ));
        } 
        else 
            {
            
            $photos = $a->photos;
            $tmp = static::$twig->loadTemplate("annonce.html.twig");

            global $config;
            $liens_photos = array();
            foreach ($photos as $key=>$p) 
            {

                $liens_photos[]="/".$config['upload_dir']."/".$p->annonce_id."/".$p->id.".".$p->extension;
            }

            $tmp->display(array(
                "annonce"=>$a,
                "photos"=>$liens_photos,
                "title"=>$a->titre));
        }
        
    }

    public function rendu_annonce()
    {

        
        $posts = static::$app->request->post();

        $tmp = static::$twig->loadTemplate("rendu_annonce.html.twig");

        $dep = \Model\Departement::find($posts['departement']);

        $_SESSION['annonce']['ville'] = $posts['ville'];
        $_SESSION['annonce']['code_postal'] = $posts['code_postal'];
        $_SESSION['annonce']['auteur'] = $posts['pseudo'];
        $_SESSION['annonce']['mail'] = $posts['mail'];
        $_SESSION['annonce']['telephone'] = $posts['telephone'];
        $_SESSION['annonce']['titre'] = $posts['titre'];
        $_SESSION['annonce']['description'] = $posts['description'];
        $_SESSION['annonce']['prix'] = $posts['prix'];
        $_SESSION['annonce']['departement']['nom'] = $dep->nom;
        $_SESSION['annonce']['departement']['id'] = $dep->id;
        $_SESSION['annonce']['categorie']['id'] = $posts['categorie'];
        $_SESSION['annonce']['mdp'] = $posts['mdp'];


        $photos = array();
        $liens_photos = array();


        foreach ($_FILES['photos']['tmp_name'] as $key => $value) 
        {
            if(!empty($value))
            {
                $explode = explode(".", $_FILES['photos']['name'][$key]);
                $extension = end($explode);
                
                global $config;

                $chemin = $config['tmp_dir'] ."/". uniqid() .".". $extension;
                
                move_uploaded_file($value, $chemin);

                $photos[] = $chemin;                

            }
        }

        $_SESSION['annonce']['photos'] = $photos;


        $tmp->display(array(
            "annonce"=>$_SESSION['annonce'],
            "photos"=>$photos,
            "action"=>static::$app->urlFor("save_annonce"),
            "method"=>"POST",
            "title"=>$_SESSION['annonce']['titre']
            ));
    }


    public function byCategorie($slug) 
    {
        $categ = \Model\Categorie::where('slug', '=', $slug)->first();

        $this->variables_menu();
        $this->variables_searchbar();


        if($categ == null) 
            {
            $tmp = static::$twig->loadTemplate('error.html.twig');
            $tmp->display(array(
                "title_error"=>"Erreur 404",
                "description"=>"La catégorie demandée n'existe pas."
            ));
        } 
        else 
            {
            $annonces = $categ->annonces()->orderBy("created_at", "desc")->with('categorie', 'departement', 'photos')->get();

            $links = array();
            $links_photos = array();
            $links_categ = array();
            global $config;
            foreach ($annonces as $key => $a) 
            {
                $links[$a->id] = static::$app->urlFor("afficher_annonce", array("id"=>$a->id) );

                if(isset($a->photos) && !$a->photos->isEmpty())
                {
                    $p = $a->photos[0];
                    $links_photos[$a->id] = "/".$config['upload_dir']."/".$p->annonce_id."/".$p->id.".".$p->extension;
                }                
                $links_categ[$a->id] = static::$app->urlFor("articles-par-categorie", array("categ_slug"=>$a->categorie->slug));
            }

            if(sizeof($annonces) == 0)
            {
                $tmp = static::$twig->loadTemplate('error.html.twig');
                $tmp->display(array(
                    "title_error"=>"Aucun article",
                    "description"=>"Cette catégorie ne contient pas encore d'articles."
                ));   
            }
            else
            {
                $tmp = static::$twig->loadTemplate('liste_annonces.html.twig');
                $tmp->display(array(
                    "title"=>$categ->nom,
                    "annonces"=>$annonces,
                    "categorie"=>$categ,
                    "links"=>$links,
                    "photos"=>$links_photos,
                    "links_categ"=>$links_categ
            
            ));

           }
        }
    }

    
    public function liste() {

        $this->variables_menu();
        $this->variables_searchbar();

        $annonces = \Model\Annonce::orderBy("created_at", "desc")->with('categorie', 'departement', 'photos')->get();

        $links = array();
        $links_photos = array();
        $links_categ = array();


        if (sizeof($annonces) < 1 ) {
            $tmp = static::$twig->loadTemplate('error.html.twig');
            $tmp->display(array(
                "title_error" => "Aucune annonce",
                "description" => "Il n'y a pas d'annonces disponibles"
            ));
        } else {


            global $config;
            foreach ($annonces as $key => $a) 
            {
                $links[$a->id] = static::$app->urlFor("afficher_annonce", array("id"=>$a->id) );

                if(isset($a->photos) && !$a->photos->isEmpty())
                {
                    
                    $p = $a->photos[0];
                    $links_photos[$a->id] = "/".$config['upload_dir']."/".$p->annonce_id."/".$p->id.".".$p->extension;
                }

                $links_categ[$a->id] = static::$app->urlFor("articles-par-categorie", array("categ_slug"=>$a->categorie->slug));
                
            }


            $tmp = static::$twig->loadTemplate('liste_annonces.html.twig');
            $tmp->display(array(
                "title" => 'Toutes les annonces',
                "annonces" => $annonces,
                "links"=>$links,
                "photos"=>$links_photos,
                "links_categ"=>$links_categ
            ));
        }
    }




    public function ajouter_formulaire()
    {
        $this->variables_menu();
        $this->variables_searchbar();

        $tmp = static::$twig->loadTemplate('formulaire_annonce.html.twig');

        $tmp->display(array(
            "departements"=>\Model\Departement::orderBy('nom')->get(),
            "categories"=>\Model\Categorie::orderBy('nom')->get(),
            "action"=>static::$app->urlFor("save_annonce"),
            "method"=>"post",
            "nom_form"=>"Déposer une annonce",
            "title"=>"Déposer une annonce"
            ));  
    }

    public function save()
    {   
        $hash = "sha256";
        global $config;
        $posts = static::$app->request()->post();
        
        $a = new \Model\Annonce();

        //Si on a prévisualisé avant, les données sont dans la session
        if(isset($_SESSION['annonce']))
        {
            $a->titre = strip_tags($_SESSION['annonce']['titre']);
            $a->description = strip_tags($_SESSION['annonce']['description']);
            $a->prix = strip_tags($_SESSION['annonce']['prix']);
            $a->code_postal = strip_tags($_SESSION['annonce']['code_postal']);
            $a->ville = strip_tags($_SESSION['annonce']['ville']);
            $a->auteur = strip_tags($_SESSION['annonce']['auteur']);
            $a->mail = strip_tags($_SESSION['annonce']['mail']);
            $a->telephone = strip_tags($_SESSION['annonce']['telephone']);
            $a->id_dep = $_SESSION['annonce']['departement']['id'];
            $a->categ_id = $_SESSION['annonce']['categorie']['id'];
            $a->mdp = hash($hash, $_SESSION['annonce']['mdp']);

            $a->save();

            if(isset($_SESSION['annonce']['photos']))
            {
                foreach ($_SESSION['annonce']['photos'] as $key => $value) 
                {
                    
                    $this->sauvegarde_photos($value, $config['upload_dir']."/",$a->id );
                }
            }

            
        }
        //Sinon dans le post
        else
        {

            $this->verifier_formulaire_annonce($posts);
            $a->titre = strip_tags($posts['titre']);
            $a->description = strip_tags($posts['description']);
            $a->prix = strip_tags($posts['prix']);
            $a->code_postal = strip_tags($posts['code_postal']);
            $a->ville = strip_tags($posts['ville']);
            $a->auteur = strip_tags($posts['pseudo']);
            $a->mail = strip_tags($posts['mail']);
            $a->telephone = strip_tags($posts['telephone']);
            $a->id_dep = $posts['departement'];
            $a->categ_id = $posts['categorie'];
            $a->mdp = hash($hash, $posts['mdp']);

            $a->save();

            foreach ($_FILES['photos']['name'] as $key => $name) 
            {
                if(!empty($name))
                {
                    $explode = explode(".",$name);
                    $extension = end($explode);
                    $this->sauvegarde_photos($_FILES['photos']['tmp_name'][$key],$config['upload_dir']."/", $a->id, $extension);


                }
            }
        } 

        if(isset($_SESSION['annonce'])) 
            unset($_SESSION['annonce']);

        $a->save();


        
        

        static::$app->redirect(static::$app->urlFor("afficher_annonce", array("id"=>$a->id)));
        

    }

    private function sauvegarde_photos($chemin, $destination, $annonce_id, $extension = null)
    {
        $p = new \Model\Photo();
        if($extension == null)
        {
            $explode = explode(".", $chemin);
            $p->extension = end($explode);
        }
        else
            $p->extension = $extension;
        $p->annonce_id = $annonce_id;
        $p->save();
        $destination .= $p->annonce_id;
        
        if(!file_exists($destination))
            mkdir($destination);
        $destination .= "/$p->id.$p->extension";
        
        
        if(is_uploaded_file($chemin))
            move_uploaded_file($chemin, $destination);
        else
            rename($chemin, $destination);           
    }

    public function modifier_formulaire_auth($id)
    {

        $this->variables_menu();
        $this->variables_searchbar();

        $a = \Model\Annonce::find($id);

        if ($a == null) {
            $tmp = static::$twig->loadTemplate("error.html.twig");
            $tmp->display(array(
                "title_error" => "Erreur 404",
                "description" => "Vous essayez de modifier une annonce qui n'existe pas."
            ));
        }
        else
        {
            $tmp = static::$twig->loadTemplate('auth.html.twig');
            $tmp->display(array(
                "action" => static::$app->urlFor("modifier_form", array("id"=>$id)),
                "method" => "post",
                "annonce" => $a,
                "message"=> "Entrez le mot de passe lié à votre annonce pour pouvoir la modifier.",
                "title"=>"Authentifiez vous"
            ));
        }

    }

    private function token()
    {
        return hash("sha256", uniqid());
    }

    private function verifier_token($token)
    {

        if(isset($_SESSION['token']) && isset($_SESSION['token_time']))
        {
            if($_SESSION['token'] == $token)
            {
                //expire au bout de 10min
                if($_SESSION['token_time'] >= (time() - 10*60))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }

    public function modifier_formulaire()
    {
        $this->variables_menu();
        $this->variables_searchbar();

        $posts = static::$app->request()->post();
        //Vérification de l'utilisateur

        try
        {
            if(!isset($posts['id_annonce']) || empty($posts['id_annonce']))
                throw new Exception("Aucune annonce spécifiée");

            $id = $posts['id_annonce'];

            $a = \Model\Annonce::find($id);

            if($a==null)
                throw new Exception("L'annonce n'existe pas");

            $formulaire = $a->toArray();

            $token = $this->token();

            $formulaire['token'] = $token;

            $_SESSION['token'] = $token;
            $_SESSION['token_time'] = time();

            $tmp = static::$twig->loadTemplate('formulaire_annonce.html.twig');



            $tmp->display(array(
                "departements"=>\Model\Departement::orderBy('nom')->get(),
                "categories"=>\Model\Categorie::orderBy('nom')->get(),
                "action"=>static::$app->urlFor("update_annonce", array("id"=>$id)),
                "method"=>"post",
                "nom_form"=>"Modifier votre annonce",
                "title"=>"Modifier une annonce",
                "form"=>$formulaire
                ));
        }
        catch(Exception $e)
        {

        }




        
    }

    public function update($id)
    {
        $posts = static::$app->request()->post();
        

        try
        {
            $this->verifier_formulaire_annonce($posts, false);

            if($this->verifier_token($posts['token']))
            {
                
                $a = \Model\Annonce::find($id);

                

                $a->id_dep = $posts['departement'];
                $a->ville = $posts['ville'];
                $a->code_postal = $posts['code_postal'];
                $a->auteur = $posts['pseudo'];
                $a->mail = $posts['mail'];
                $a->telephone = $posts['telephone'];
                $a->titre = $posts['titre'];
                $a->categ_id = $posts['categorie'];
                $a->description = $posts['description'];
                $a->prix = $posts['prix'];


                $a->update();

                static::$app->redirect(static::$app->urlFor("afficher_annonce", array("id"=>$id)));
            }
            else
                throw new \Exception("Erreur d'authentification de la requete");


        }
        catch(\Exception $e)
        {

            
            $tmp = static::$twig->loadTemplate('error.html.twig');
            $tmp->display(array(
                "title_error"=>"Erreur dans le formulaire",
                "description"=>$e->getMessage()
                ));
        }



    }


    public function supprimer_formulaire($id) {


        $this->variables_menu();
        $this->variables_searchbar();

        $a = \Model\Annonce::find($id);

        if ($a == null) {
            $tmp = static::$twig->loadTemplate("error.html.twig");
            $tmp->display(array(
                "title_error" => "Erreur 404",
                "description" => "Vous essayez de supprimer une annonce qui n'existe pas."
            ));
        } else {
            $tmp = static::$twig->loadTemplate('auth.html.twig');
            $tmp->display(array(
                "action" => static::$app->urlFor("supprimer_annonce"),
                "method" => "post",
                "annonce" => $a,
                "message"=> "Entrez le mot de passe lié à votre annonce afin de confirmer la suppression.",
                "title"=>"Authentifiez vous"
            ));
        }
    }

    public function delete() {
        $this->variables_menu();
        $this->variables_searchbar();
        global $config;

        $posts = static::$app->request()->post();
        $mdp = hash("sha256", $posts['mdp']);
        $a = \Model\Annonce::find($posts['id_annonce']);
        try {
            if ($a == null)
                throw new \Exception('Vous essayez de supprimer une annonce qui n\'existe pas');
            if ($a->mdp != $mdp)
                throw new \Exception('Votre mot de passe est erroné');

            $photos = $a->photos()->get();
            $dir = $config['upload_dir'] ."/". $a->id;
            if (is_dir($dir)) {
                foreach ($photos as $photo) {
                    $file = $dir . '/' . $photo->id . '.' . $photo->extension;

                    if (file_exists($file)) {
                        \unlink($file);
                    }
                    $photo->delete();
                }
            } else {
                foreach ($photos as $photo) {

                    $photo->delete();
                }
            }
            $a->delete();

            $tmp = static::$twig->loadTemplate('supprimer_annonce.html.twig');
            $tmp->display(array(
                "message" => "Mes Annonces",
                "status" => "L'annonce a été bien supprimée",
                "route" => "/mes_annonces"
            ));
        } catch (\Exception $e) {
            $tmp = static::$twig->loadTemplate('supprimer_annonce.html.twig');
            $tmp->display(array(
                "message" => "Retour au formulaire",
                "status" => $e->getMessage(),
                "route" => "/annonce/supprimer/" . $a->id
            ));
        }
    }

    public function mesAnnonces() {
        $this->variables_menu();
        $this->variables_searchbar();

        $tmp = static::$twig->loadTemplate('annonces_mail_formulaire.html.twig');
        $tmp->display(array(
            "action" => static::$app->urlFor("annonces_mail"),
            "method" => "post",
            "title"=>"Entrez votre adresse mail"
        ));
    }

    public function byMail() 
    {
        $this->variables_menu();
        $this->variables_searchbar();
        $posts = static::$app->request()->post();
        $annonces= \Model\Annonce::where('mail','=',$posts["mail"])->orderBy("created_at", "desc")->with('categorie', 'departement', 'photos')->get();
        
        $links = array();
        $links_photos = array();
        $links_categ = array();

        foreach ($annonces as $key => $value) 
        {
            $links[$value->id] = static::$app->urlFor("afficher_annonce", array("id"=>$value->id) );
        }

        global $config;
        foreach ($annonces as $key => $a) 
        {
            $links[$a->id] = static::$app->urlFor("afficher_annonce", array("id"=>$a->id) );
            if(isset($a->photos) && !$a->photos->isEmpty())
            {
                $p = $a->photos[0];
                $links_photos[$a->id] = "/".$config['upload_dir']."/".$p->annonce_id."/".$p->id.".".$p->extension;

            }      
            $links_categ[$a->id] = static::$app->urlFor("articles-par-categorie", array("categ_slug"=>$a->categorie->slug));          
            
        }
        
        if(sizeof($annonces) < 1) 
            {
            $tmp = static::$twig->loadTemplate('error.html.twig');
            $tmp->display(array(
                "title_error"=>"Aucune annonce",
                "description"=>"Aucune annonce associée à cette addresse électronique"
            ));
        } 
        else 
            {
            
            $tmp = static::$twig->loadTemplate('liste_annonces.html.twig');
            $tmp->display(array(
                "title" => 'Annonces de l\'addresse électronique: '.$posts["mail"],
                "annonces" => $annonces,
                "links"=>$links,
                "photos"=>$links_photos,
                "links_categ"=>$links_categ
            ));
        }
    }

    public function byGets() {
        $this->variables_menu();
        $this->variables_searchbar();
        $preremp = array();
        
        $gets = static::$app->request()->get();
        
        $annonces = \Model\Annonce::orderBy("created_at", "desc")->with('categorie', 'departement', 'photos');

        if ($gets["title"] != "") {
            $preremp['nom'] = $gets['title'];
            $annonces->where(function($q) use ($gets) {
                $q->where("titre", "like", "%" . $gets["title"] . "%");
                $q->orwhere("description", "like", "%" . $gets["title"] . "%");
            });
        }
        if ($gets["prix"] != 0) {
            $preremp['prix'] = $gets["prix"];
            $annonces->where("prix", "<=", $gets["prix"]);
        }
        if ($gets["categorie"] != 0) {
            $preremp['categ_select'] = $gets["categorie"];
            $annonces->where("categ_id", "=", $gets["categorie"]);
        }
        if ($gets["departement"] != 0) {
            $preremp['depart_select'] = $gets['departement'];
            $annonces->where("id_dep", "=", $gets["departement"]);
        }
        if ($gets["ville"] != "") {
            $preremp['ville'] = $gets['ville'];
            $annonces->where("ville", "like", "%".$gets["ville"]."%");
        }
        
        $annonces=$annonces->get();//recuperer les donnees avec ma methode get
        $links = array();
        $links_photos = array();
        $links_categ = array();

        foreach ($annonces as $key => $value) 
            {
            $links[$value->id] = static::$app->urlFor("afficher_annonce", array("id"=>$value->id) );
        }

        global $config;
        foreach ($annonces as $key => $a) 
        {
            $links[$a->id] = static::$app->urlFor("afficher_annonce", array("id"=>$a->id) );
            if(isset($a->photos) && !$a->photos->isEmpty())
            {
                $p = $a->photos[0];
                $links_photos[$a->id] = "/".$config['upload_dir']."/".$p->annonce_id."/".$p->id.".".$p->extension;
            }     
            $links_categ[$a->id] = static::$app->urlFor("articles-par-categorie", array("categ_slug"=>$a->categorie->slug));           
            
        }

        if (sizeof($annonces) < 1 ) {
            $tmp = static::$twig->loadTemplate('error.html.twig');
            $tmp->display(array(
                "preremp"=>$preremp,
                "title_error" => "Aucun résulat",
                "description" => "Il n'y a pas des annonces disponibles"
            ));
        } else {
            $tmp = static::$twig->loadTemplate('liste_annonces.html.twig');
            $tmp->display(array(
                "title" => 'Toutes les annonces',
                "annonces" => $annonces,
                "links"=>$links,
                "photos"=>$links_photos,
                "preremp"=>$preremp,
                "links_categ"=>$links_categ

            ));
        }
    }

}
