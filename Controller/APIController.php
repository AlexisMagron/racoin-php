<?php

namespace Controller;

class APIController extends AbstractController {

    public $gets;

    public function url_decoder($array) {
        array_walk_recursive($array, function(&$item, $key) {

            $item = urldecode($item);
        });

        return $array;
    }

    public function setHeaders() {
        static::$app = \Slim\Slim::getInstance();
        static::$app->response->headers->set('Content-Type', 'application/json');
    }

    public function authentification() {
        static::$app = \Slim\Slim::getInstance();
        $this->gets = static::$app->request->get();
        try {
            if (!isset($this->gets['key']) || empty($this->gets['key'])) {
                throw new \Exception("L'API key n'a pas été trouvée");
            }
            $key = $this->gets["key"];
            $api = \Model\API::all();
            for ($i = 0; $i < sizeof($api); $i++) {
                if ($api[$i]["key"] != $key) {
                    throw new \Exception("L'API key n'est pas valide.");
                }
            }
        } catch (\Exception $e) {
            echo json_encode(array("status" => "erreur", "message" => $e->getMessage()));
            static::$app->stop();
        }
    }

    public static function verifier_annonce($posts, $mdp = true) {

        if ($mdp) {

            if (!isset($posts['mdp']) || empty($posts['mdp']))
                throw new \Exception("Il manque le mot de passe");

            if (strlen($posts['mdp']) < 6)
                throw new \Exception("Votre mot de passe est trop court");

            if ($posts['mdp'] != $posts['confirm_mdp'])
                throw new \Exception("Les mots de passe ne correspondent pas");
        }

        if (!isset($posts['departement']) || empty($posts['departement']))
            throw new \Exception("Département invalide");

        if (!isset($posts['ville']) || empty($posts['ville']))
            throw new \Exception("Il manque la ville");

        if (!isset($posts['code_postal']) || empty($posts['code_postal']) || !is_numeric($posts['code_postal']))
            throw new \Exception("Code postal invalide");

        if (!isset($posts['pseudo']) || empty($posts['pseudo']))
            throw new \Exception("Il manque votre pseudonyme");

        if (!isset($posts['mail']) || empty($posts['mail']))
            throw new \Exception("Il manque votre e-mail");

        if (!isset($posts['telephone']) || empty($posts['telephone']))
            throw new \Exception("Il manque votre numéro de téléphone");

        if (!isset($posts['titre']) || empty($posts['titre']))
            throw new \Exception("Il manque un titre à votre annonce !");

        if (!isset($posts['categorie']) || empty($posts['categorie']))
            throw new \Exception("Catégorie invalide");

        if (!isset($posts['prix']) || empty($posts['prix']) || !is_numeric($posts['prix']))
            throw new \Exception("Prix invalide !");

        if (!isset($posts['description']))
            $posts['description'] = '';
    }

///methodes
    public function urlDispo() {
        $this->setHeaders();
        $this->authentification();
        try {
            $links[0] = array("message" => "Page d'accueil de l'API", "link" => static::$app->urlFor("api_accueil"), "methode" => "get");
            $links[1] = array("message" => "Liste de Categories disponibles", "link" => static::$app->urlFor("api_categories_list"), "methode" => "get");
            $links[2] = array("message" => "Liste d'annonces par categorie", "link" => static::$app->urlFor("api_annonces_categorie"), "methode" => "get");
            $links[3] = array("message" => "Liste d'annonces recherches par categories", "Rechercher par" => array("Titre", "Description", "Prix Maximum", "Prix Minimum", "Categorie", "Despartement", "Ville"), "link" => static::$app->urlFor("api_search"), "methode" => "get");
            $links[4] = array("message" => "Liste d'annonces", "link" => static::$app->urlFor("api_annonces"), "methode" => "get");
            $links[5] = array("message" => "Afficher une annonce", "link" => static::$app->urlFor("api_afficher_annonce"), "methode" => "get");
            $links[6] = array("message" => "Ajouter une annonce", "link" => static::$app->urlFor("api_annonces_ajouter"), "methode" => "post");
            $links[7] = array("message" => "Modifier une annonce", "link" => static::$app->urlFor("api_annonces_modifier"), "methode" => "put");
            $a = array("status" => "Success", "message" => "Bienvenu sur l'API du Racoin. Voici la liste d'url disponibles.", "url" => $links);
            echo json_encode($a);
        } catch (\Exception $e) {
            echo json_encode(array("status" => "erreur", "message" => $e->getMessage()));
        }
    }

    public function listeAnnonce() {
        $this->setHeaders();
        try {

            $this->authentification();

            $colonnes = array('id', 'titre', 'prix');
            $a = \Model\Annonce::orderBy("created_at", "desc")->get($colonnes)->toArray();
            if (sizeof($a) < 1) {
                throw new \Exception("Il n'y a pas des annonces disponibles");
            }
            for ($i = 0; $i < sizeof($a); $i++) {
                $a[$i]["links"]["self"]["href"] = static::$app->urlFor("api_afficher_annonce", array("id" => $a[$i]['id']));
            }
            echo json_encode($a);
        } catch (\Exception $e) {
            echo json_encode($this->url_decoder(array("status" => "erreur", "message" => $e->getMessage())));
        }
    }

    public function afficherAnnonce($id) {
        $this->setHeaders();
        try {
            $this->authentification();
            $a = \Model\Annonce::with('departement', 'photos', 'categorie')->get()->find($id);
            if (!isset($a) || empty($a)) {
                throw new \Exception("Vous essayez d'accéder à une annonce qui n'existe pas.");
            }
            global $config;
            $i = 0;
            $c=[];
            $photos = $a["photos"];
            foreach ($photos as $key => $p) {
                $c["links"][$i]["self"]["href"] = "/" . $config['upload_dir'] . "/" . $p["annonce_id"] . "/" . $p["id"] . "." . $p["extension"];
                $i++;
            }
            unset($a["photos"]);
            $a["photos"] = $c;
            $b["self"]["href"] = static::$app->urlFor("api_annonces_categorie", array("categorie" => $a["categorie"]["nom"]));
            $a["categorie"]["links"] = $b;

            unset($a["mdp"], $a["created_at"], $a["updated_at"], $a["id_dep"], $a["categ_id"], $a["categorie"]["slug"]); //retirer information de l'array
            echo json_encode($a);
        } catch (\Exception $e) {
            echo json_encode($this->url_decoder(array("status" => "erreur", "message" => $e->getMessage())));
        }
    }

    public function byCategorie($slug) {
        $this->setHeaders();
        try {
            $this->authentification();
            $categ = \Model\Categorie::where('slug', '=', $slug)->first();
            $colonnes = array('id', 'titre', 'prix');
            if ($categ == NULL) {
                throw new \Exception("Vous essayez d'accéder à une catègorie qui n'existe pas.");
            }
            $a = $categ->annonces()->orderBy("created_at", "desc")->get($colonnes);

            if (sizeof($a) < 1) {
                throw new \Exception("Il n'y a pas des annonces disponibles pour cette catègorie");
            }

            for ($i = 0; $i < sizeof($a); $i++) {
                $c["self"]["href"] = static::$app->urlFor("api_afficher_annonce", array("id" => $a[$i]['id']));

                $a[$i]["links"] = $c;
            }

            echo json_encode($a);
        } catch (\Exception $e) {
            echo json_encode(array("status" => "erreur", "message" => $e->getMessage()));
        }
    }

    public function listeCategories() {
        $this->setHeaders();
        try {
            $this->authentification();
            $colonnes = array('id', 'nom', 'slug');
            $categories = \Model\Categorie::all($colonnes);
            if ($categories == NULL) {
                throw new \Exception("Il n'y a pas des catègories disponibles.");
            }
            $i = 0;
            foreach ($categories as $key => $c) {
                $d["self"]["href"] = static::$app->urlFor("api_annonces_categorie", array("categorie" => $c["slug"]));
                $categories[$i]["links"] = $d;
                $i++;
            }
            echo json_encode($categories);
        } catch (\Exception $e) {
            echo json_encode(array("status" => "erreur", "message" => $e->getMessage()));
        }
    }

    public function byGets() {
        $this->setHeaders();
        $this->authentification();
        $colonnes = array('id', 'titre', 'prix');
        $gets = $this->gets;
        try {

            $annonces = \Model\Annonce::orderBy("created_at", "desc")->with('categorie', 'departement', 'photos');

            if (isset($gets["title"]) && $gets["title"] != "") {
                $annonces->where(function($q) use ($gets) {
                    $q->where("titre", "like", "%" . $gets["title"] . "%");
                    $q->orwhere("description", "like", "%" . $gets["title"] . "%");
                });
            }
            if (isset($gets["prixMax"]) && $gets["prixMax"] != 0) {
                $annonces->where("prix", "<=", $gets["prixMax"]);
            }
            if (isset($gets["prixMin"]) && $gets["prixMin"] != 0) {
                $annonces->where("prix", ">=", $gets["prixMin"]);
            }
            if (isset($gets["categorie"]) && $gets["categorie"] != 0) {
                $annonces->where("categ_id", "=", $gets["categorie"]);
            }
            if (isset($gets["departement"]) && $gets["departement"] != 0) {
                $annonces->where("id_dep", "=", $gets["departement"]);
            }
            if (isset($gets["ville"]) && $gets["ville"] != "") {
                $annonces->where("ville", "like", "%" . $gets["ville"] . "%");
            }
            $a = $annonces->get($colonnes); //recuperer les donnees avec la methode get

            for ($i = 0; $i < sizeof($a); $i++) {
                $c["self"]["href"] = static::$app->urlFor("api_afficher_annonce", array("id" => $a[$i]['id']));
                $a[$i]["links"] = $c;
                unset($a[$i]["categorie"], $a[$i]["departement"], $a[$i]["photos"]);
            }
            if (sizeof($a) < 1) {
                throw new \Exception("Il n'y a pas d'annonces disponibles");
            }
            echo json_encode($a);
        } catch (\Exception $e) {
            echo json_encode(array("status" => "erreur", "message" => $e->getMessage()));
        }
    }

    private function sauvegarde_photos($chemin, $destination, $annonce_id, $extension = null) {

        $p = new \Model\Photo();

        if ($extension == null) {

            $explode = explode(".", $chemin);
            $p->extension = end($explode);
        } else {

            $p->extension = $extension;
        }
        $p->annonce_id = $annonce_id;
        $p->save();
        $destination .= $p->annonce_id;

        if (!file_exists($destination))
            mkdir($destination);
        $destination .= "/$p->id.$p->extension";

        if (is_uploaded_file($chemin))
            move_uploaded_file($chemin, $destination);
        else
            rename($chemin, $destination);
    }

    public function save() {
        $this->setHeaders();
        try {
            $this->authentification();
            $posts = $this->url_decoder(static::$app->request->post());
            global $config;
            $gets = $this->gets;
            $hash = "sha256";
            $a = new \Model\Annonce();

            $this->verifier_annonce($posts);

            $a->titre = strip_tags($posts['titre']);
            $a->description = strip_tags($posts['description']);
            $a->prix = strip_tags($posts['prix']);
            $a->code_postal = strip_tags($posts['code_postal']);
            $a->ville = strip_tags($posts['ville']);
            $a->auteur = strip_tags($posts['pseudo']);
            $a->mail = strip_tags($posts['mail']);
            $a->telephone = strip_tags($posts['telephone']);
            $a->id_dep = $posts['departement'];
            $a->categ_id = $posts['categorie'];
            $a->mdp = hash($hash, $posts['mdp']);
            $a->save();
            if (isset($_FILES['photos'])) {
                foreach ($_FILES['photos']['name'] as $key => $name) {
                    if (!empty($name)) {
                        $explode = explode(".", $name);
                        $extension = end($explode);
                        $this->sauvegarde_photos($_FILES['photos']['tmp_name'][$key], $config['upload_dir'] . "/", $a->id, $extension);
                    }
                }
            }
            if ($a->save()) {
                echo json_encode(array("status" => "success", "message" => "L'annonce a été bien ajoutée"));
            } else {
                throw new \Exception("Service indisponible");
            }
        } catch (\Exception $e) {
            echo json_encode(array("status" => "erreur", "message" => $e->getMessage()));
        }
    }

    public function update($id) {
        $this->setHeaders();
        try {
            $posts = static::$app->request()->post();
            $this->authentification();
            $this->verifier_annonce($posts, false);

            $a = \Model\Annonce::find($id);
            $mdp = hash("sha256", $posts['mdp']);
            if ($mdp != $a->mdp) {
                throw new \Exception("Votre mot de passe est erroné");
            }
            $a->id_dep = $posts['departement'];
            $a->ville = $posts['ville'];
            $a->code_postal = $posts['code_postal'];
            $a->auteur = $posts['pseudo'];
            $a->mail = $posts['mail'];
            $a->telephone = $posts['telephone'];
            $a->titre = $posts['titre'];
            $a->categ_id = $posts['categorie'];
            $a->description = $posts['description'];
            $a->prix = $posts['prix'];

            if ($a->update()) {
                echo json_encode(array("status" => "success", "message" => "L'annonce a été bien modifiée"));
            }
        } catch (\Exception $e) {
            echo json_encode(array("status" => "erreur", "message" => $e->getMessage()));
        }
    }

}
