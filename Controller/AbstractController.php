<?php
namespace Controller;

abstract class AbstractController
{
	protected static $app = null;
	protected static $twig = null;



	public function __construct()
	{		
		
	}

	public static function setSlim($app)
	{
		static::$app = $app;
	}

	public static function setTwig($twig)
	{
		static::$twig = $twig;
	}

	protected function variables_menu()
	{
		$m = array(
			array("name"=>"Liste d'annonces", "href"=> static::$app->urlFor("root")),
			

			array("name"=>"Ajouter une annonce", "href" => static::$app->urlFor("ajouter_annonce")),

            array("name"=>"Mes Annonces", "href"=> static::$app->urlFor("annonces_mail_formulaire")),

            array("name"=>"Inscription", "href"=> static::$app->urlFor("inscription"))


			);
		static::$twig->addGlobal("menu", $m);
	}

	protected function variables_searchbar()
	{
		$searchbar = array(
			"action"=> static::$app->urlFor("search"),
			"categories" => \Model\Categorie::orderBy("nom")->get(),
			"departements" => \Model\Departement::orderBy("nom")->get()
			);


		static::$twig->addGlobal("searchbar", $searchbar);

	}
}