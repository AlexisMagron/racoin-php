<?php

namespace Controller;

class UsersController extends AbstractController
{

	public function inscription_formulaire($preremp = null, $message = null)
	{
		$categories = \Model\Categorie::all();
		$departements = \Model\Departement::all();
		

		$this->variables_menu();
		$this->variables_searchbar();
		$tmp = static::$twig->loadTemplate('inscription.html.twig');
		$tmp->display(array(
			"categories"=>$categories,
			"departements"=>$departements,
			"action"=>static::$app->urlFor("finish_inscription"),
			"method"=>"post",
			"title"=>"Inscription",
			"preremp"=>$preremp,
			"message"=>$message
			));
	}

	public function inscription()
	{
		$post = static::$app->request()->post();

		try {
				if(!isset($post['departement']))
					throw new \Exception("Choisissez un département");
				if(!isset($post['categ']))
					throw new \Exception("Choisissez votre catégorie préférée");

			\Auth\Auth::createUser(
				$post['email'],
				$post['mdp'],
				$post['confirm_mdp'],
				$post['pseudo'],
				$post['prenom'],
				$post['nom'],
				$post['telephone'],
				$post['adresse'],
				$post['ville'],
				$post['departement'],
				$post['categ']
				);

			static::$app->redirect(static::$app->urlFor("list_annonces"));

		} catch (\Exception $e) {

			$this->inscription_formulaire($post, $e->getMessage());
		}

	}

	public function authentication()
	{
		$post = static::$app->request()->post();

		if(isset($post['email']) && isset($post['mdp']) && isset($post['current_url']))
		{
			\Auth\Auth::loadProfile($post['email'], $post['mdp']);

			static::$app->redirect($post['current_url']);

		}
	}

	public function disconnect()
	{
		$get = static::$app->request()->get();
		session_destroy();
		if(isset($get['current_url']))
		{
			static::$app->redirect($get['current_url']);
		}
		else
			static::$app->redirect(static::$app->urlFor("root"));
	}


	public function profil($id)
	{
		if(!\Auth\Auth::isLoged())
			static::$app->redirect(static::$app->urlFor("root"));
		$this->variables_menu();
		$this->variables_searchbar();

		$u = \Model\Users::find($id);

		if($u == null)
		{
			$tmp = static::$twig->loadTemplate("error.html.twig");
			$tmp->display(array(
				"title_error"=>"Erreur",
				"description"=>"Le profil demandé n'existe pas"
				));
		}
		else
		{

			$tmp = static::$twig->loadTemplate("profil.html.twig");
			$tmp->display(array(
				"title"=>"Votre profil",
				"user"=>$u
				));
		}
	}








}