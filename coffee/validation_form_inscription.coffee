$(document).ready(->

    validator = new FormValidator('form_inscription', [
            {
                name:'mail',
                display:'Adresse email',
                rules:'required'
            },
            {
                name:'pseudo',
                display:'Pseudo',
                rules:'required'
            },
            {
                name:'prenom',
                display: 'Prénom',
                rules:'required'
            },
            {
                name:'nom',
                display: 'Nom',
                rules:'required'
            },
            {
                name:'telephone',
                display:'Téléphone',
                rules:'required'
            },
            {
                name:'adresse',
                display: 'Adresse',
                rules:'required'
            },
            {
                name:'ville',
                display: 'Ville',
                rules:'required'
            },
            {
                name:'mdp',
                display: 'Mot de passe',
                rules:'required'
            },
            {
                name:'confirm_mdp',
                display: 'confirmation du mot de passe',
                rules: 'required|matches[mdp]|min_length[6]'
            }


        ] , (errors, event)->
                if(errors.length > 0)
                    $(".error").remove()
                    for error in errors
                        $("<span>").text(error.message).attr("class", "error").insertBefore(".boutons")
                
                

                

        )




    validator.setMessage("required", "Le champ %s est requis.")
    validator.setMessage("matches", "La %s n'est pas valide")
    validator.setMessage("min_length", "Le champ %s doit contenir au moins %s caractères")




    
)