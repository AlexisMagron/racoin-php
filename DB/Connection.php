<?php
namespace DB;

use Illuminate\Database\Capsule\Manager as Capsule;



class Connection
{
	
	private static $capsule;
	public static function demarrer($nom_fichier)
	{
		$param = parse_ini_file($nom_fichier);

		static::$capsule = new Capsule();
		static::$capsule->addConnection($param);		
		static::$capsule->setAsGlobal();		
		
		static::$capsule->bootEloquent();
	}

	public static function logs()
	{
		return Capsule::getQueryLog();
	}


}