<?php 

namespace Auth;

class Auth
{

	public static function createUser($email, $password, $confirm_password, $pseudo, $prenom, $nom, $telephone, $adresse, $ville, $id_dep, $categ_id)
	{		

		if(\Model\Users::where("email", "=", $email)->exists())
			throw new \Exception("Adresse e-mail déjà utilisée");

		if(strlen($password) < 6)
			throw new \Exception("Le mot de passe doit faire au minimum 6 caractères");

		if($password != $confirm_password)
			throw new \Exception("Les mots de passe ne correspondent pas");

		if(empty($email) or empty($pseudo) or empty($nom) or empty($prenom) or empty($telephone) or empty($adresse) or empty($ville) or empty($id_dep) or empty($categ_id))
			throw new \Exception("Un ou plusieurs champs n'ont pas été remplis");


		$u = new \Model\Users();

		$u->pseudo = $pseudo;
		$u->email = $email;
		$u->nom = $nom;
		$u->prenom = $prenom;
		$u->telephone = $telephone;
		$u->adresse = $adresse;
		$u->ville = $ville;
		$u->id_dep = $id_dep;
		$u->categ_id = $categ_id;


		$u->password = password_hash($password, PASSWORD_BCRYPT);

		$u->save();

	}

	public static function loadProfile($email, $password)
	{
		$u = \Model\Users::where("email", "=", $email)->first();

		if($u->exists())
		{

			if(password_verify($password, $u->password))
			{
				session_destroy();
				session_start();
				$_SESSION['user'] = $u->toArray();
			}

			
			
		}
		
	}

	public static function isLoged()
	{	
		return (isset($_SESSION['user']));	
	}
	
	public static function getUser()
	{
		if(!isset($_SESSION['user']['id']))
			return null;
		else
			return \Model\Users::find($_SESSION['user']['id']);
	}
}