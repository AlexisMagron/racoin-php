##Installation

#Vhost
Ajouter le virtualhost sur apache ou sur wampserver (racoin.conf)

#Mysql
Les scripts de base de données se trouve dans le dossier 'base'. Il y a un script pour créer la base et un script donnees.sql qui permet d'insérer quelques données pour utiliser l'application,  comme des catégories ainsi qu'une clé pour utiliser l'api.

#Connexion à la base
Renommez le fichier config.default.ini en config.ini en n'oubliant pas d'indiquer les paramètres de connexion à la base de données.

#Api
clé par défaut créée par le script sql : 9C410C648933FA616D828FE65BF8CC5163A1C5AD

url du type : 
http://racoin.alexismagron.fr/api/annonces/1?key=<clé>




