<?php
namespace Model;

class Annonce extends \Illuminate\DataBase\Eloquent\Model
{
	protected $table = "annonce";
	protected $primaryKey = "id";
	public $timestamps = true;

	public function departement()
	{
		return $this->belongsTo("Model\Departement", "id_dep");
	}

	public function categorie()
	{
		return $this->belongsTo("Model\Categorie", "categ_id");
	}

	public function photos()
	{
		return $this->hasMany("Model\Photo", "annonce_id");
	}


	
}