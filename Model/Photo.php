<?php
namespace Model;

class Photo extends \Illuminate\DataBase\Eloquent\Model
{
	protected $table = "photo";
	protected $primaryKey = "id";
	public $timestamps = false;
	
}