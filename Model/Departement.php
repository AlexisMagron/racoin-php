<?php
namespace Model;

class Departement extends \Illuminate\DataBase\Eloquent\Model
{
	protected $table = "departement";
	protected $primaryKey = "id";
	public $timestamps = false;
	
}