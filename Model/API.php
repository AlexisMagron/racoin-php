<?php
namespace Model;

class API extends \Illuminate\DataBase\Eloquent\Model
{
	protected $table = "api";
	protected $primaryKey = "id";
	public $timestamps = false;
}