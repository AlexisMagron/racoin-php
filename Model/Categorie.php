<?php
namespace Model;

class Categorie extends \Illuminate\DataBase\Eloquent\Model
{
	protected $table = "categorie";
	protected $primaryKey = "id";
	public $timestamps = false;

	public function annonces()
	{
		return $this->hasMany('Model\Annonce', 'categ_id');
	}
	
}