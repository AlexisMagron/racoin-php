<?php
session_start();


require_once "vendor/autoload.php";
require_once "config.php";

DB\Connection::demarrer("config.ini");

$app = new Slim\Slim();
$twig = new \Twig_Environment(new \Twig_Loader_Filesystem("templates"), array('debug'=>true));



Controller\AbstractController::setSlim($app);
Controller\AbstractController::setTwig($twig);


$app->hook('slim.before', function () use ($app, $twig) 
{



    //Url courante
    $current_url = $app->request->getResourceUri();

    //Si un utilisateur se connecte on pourra le renvoyer vers la page ou il se trouvait
    $twig->addGlobal("current_url", $current_url);

      

    if(Auth\Auth::isLoged())
    {
        $u = Auth\Auth::getUser();
        $twig->addGlobal("user", $u);
        $twig->addGlobal("disconnect", $app->urlFor("disconnect")."?current_url=$current_url");
        $twig->addGlobal("profil", $app->urlFor("profil",array("id"=>$u->id)));
    }
    else
        $twig->addGlobal("authentication_action", $app->urlFor("authentication"));  

});



//Accueil
$app->get("/", function() use ($app)
{
	$app->redirect($app->urlFor("list_annonces"));

})->name("root");

$app->get("/annonces", function()
{
	$c=new Controller\AnnonceController();
        $c->liste();

})->name("list_annonces");


$app->get("/annonce/:id", function($id) use ($app, $twig)
{
	$c = new Controller\AnnonceController();
	$c->afficher($id);
})->name("afficher_annonce");

$app->get("/search", function() use($app) 
{
    $c = new Controller\AnnonceController();
    $c->byGets();
})->name("search");

$app->get("/annonces/:categ_slug", function($categ_slug)
{
	$c = new Controller\AnnonceController();
	$c->byCategorie($categ_slug);
})->name("articles-par-categorie");

$app->get("/annonce/ajouter/", function()
{
	$c = new Controller\AnnonceController();
	$c->ajouter_formulaire();

})->name("ajouter_annonce");

$app->post("/annonce", function() use ($app, $twig)
{
	$c = new Controller\AnnonceController();

    $posts = $app->request->post();

    try
    {
        //Sauvegarder l'annonce
        if(isset($posts['valider']))
        {
            $c->save();
        }
        //Prévisualiser
        else if(isset($posts['rendu']))
        {
            \Controller\AnnonceController::verifier_formulaire_annonce($posts);  
            $c->rendu_annonce();
        }
        //Annule l'ajout de l'annonce
        else
        {
            if(isset($_SESSION['annonce']))
                unset($_SESSION['annonce']);
            $app->redirect($app->urlFor("ajouter_annonce"));
        }
    }
    catch(Exception $e)
    {   

        $tmp = $twig->loadTemplate('error.html.twig');
        $tmp->display(array(
            "title_error"=>"Erreur dans le formulaire",
            "description"=>$e->getMessage()
            ));
    } 
	


})->name("save_annonce");

$app->get("/annonce/modifier/:id", function($id)
{
    $c = new Controller\AnnonceController();
    $c->modifier_formulaire_auth($id);

})->name("modifier_auth");

$app->post("/annonce/modifier/:id", function($id)
{
    $c = new Controller\AnnonceController();
    $c->modifier_formulaire($id);

})->name("modifier_form");

$app->post("/annonce/:id", function($id)
{
    $c = new Controller\AnnonceController();

    $c->update($id);
})->name("update_annonce");

$app->get("/annonce/supprimer/:id", function ($id)
{
        $c=new Controller\AnnonceController();
        $c->supprimer_formulaire($id);
})->name("supprimer_annonce_formulaire");

$app->post("/annonce/supprimer/", function ()
{
        $c=new Controller\AnnonceController();
        $c->delete();
})->name("supprimer_annonce");


$app->get("/mes_annonces", function ()
{
        $c=new Controller\AnnonceController();
        $c->mesAnnonces();
})->name("annonces_mail_formulaire");

$app->post("/mes_annonces", function ()
{
        $c=new Controller\AnnonceController();
        $c->byMail();
})->name("annonces_mail");


$app->get("/inscription", function()
{
    $c = new Controller\UsersController();
    $c->inscription_formulaire();
})->name("inscription");

$app->post("/inscription", function()
{
    $c = new Controller\UsersController();
    $c->inscription();
})->name("finish_inscription");

$app->post("/authentication", function()
{
    $c = new Controller\UsersController();
    $c->authentication();

})->name("authentication");

$app->get("/disconnect", function()
{
    $c = new Controller\UsersController();
    $c->disconnect();
})->name("disconnect");

$app->get("/user/:id", function($id)
{
    $c = new Controller\UsersController();
    $c->profil($id);
})->name("profil");



//group api REST
$app->group("/api", function () use ($app) {

    $app->get("/", function () use ($app) {
        $a=new Controller\APIController();
        $a->urlDispo();
    })->name("api_accueil");
    
    $app->group("/categories", function () use ($app) {
        $app->get("/", function () {
            $a = new Controller\APIController();
            $a->listeCategories();
        })->name("api_categories_list");
        
        $app->get("/:categorie", function ($slug) {
            $a = new Controller\APIController();
            $a->byCategorie($slug);
        })->name("api_annonces_categorie");
    });

    $app->get("/search", function () {
        $a = new Controller\APIController();
        $a->byGets();
    })->name("api_search");
    
    $app->group("/annonces", function () use ($app) {
        
        $app->get("/", function () {//afficher liste d'annonces
            $a = new Controller\APIController();
            $a->listeAnnonce();
        })->name("api_annonces");
        $app->post("/", function () {//ajouter une annonce
            $a = new Controller\APIController();
            $a->save();
        })->name("api_annonces_ajouter");
        $app->put("/:id", function ($id) {//modifier  l'annonce
            $a = new Controller\APIController();
            $a->update($id);
        })->name("api_annonces_modifier");
        $app->get("/:id", function ($id) {//afficher l'annonce
            $a = new Controller\APIController();
            $a->afficherAnnonce($id);
        })->name("api_afficher_annonce");
    });
});
$app->run();

